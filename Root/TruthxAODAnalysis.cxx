#include <AsgTools/MessageCheck.h>
#include <TruthAnalysis/TruthxAODAnalysis.h>
// SG
#include <xAODJet/JetContainer.h>
#include <xAODEventInfo/EventInfo.h>
#include "xAODTruth/TruthParticle.h"
// Root
#include <TLorentzVector.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TFile.h>

TruthxAODAnalysis :: TruthxAODAnalysis (const std::string& name, ISvcLocator *pSvcLocator) : EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
  m_doVZ=false; // TRUTH1 - look at VZllbb b-hadron flavours 
  m_doVjets=true; // TRUTH3 - look at maxHT, pTV splitting


}



StatusCode TruthxAODAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  int Nbins=500; double min=0, max=500;

  // mBB
  if (m_doVZ) {
    TString mbbTit = ";#it{m}_{#it{bb}} [GeV]";
    ANA_CHECK (book (TH1F ("mbb", "mbb", Nbins, min, max))); 
    
    // HF fractions
    ANA_CHECK (book (TH1F ("bDecays", "B hadron decays", 11, 0, 11))); 
    
    // simple fragmentation function estimates
    ANA_CHECK (book (TH1F ("bPtFrac", "pT B / pT jet", 60, 0, 1.2))); 
    ANA_CHECK (book (TH1F ("bPtFrac 0", "pT B / pT jet", 60, 0, 1.2))); 
    ANA_CHECK (book (TH1F ("bPtFrac 1", "pT B / pT jet", 60, 0, 1.2))); 
    ANA_CHECK (book (TH1F ("bPtFrac 2", "pT B / pT jet", 60, 0, 1.2))); 
    ANA_CHECK (book (TH1F ("bPtFrac 3", "pT B / pT jet", 60, 0, 1.2))); 
    ANA_CHECK (book (TH1F ("bPtFrac 4", "pT B / pT jet", 60, 0, 1.2))); 
    ANA_CHECK (book (TH1F ("bPtFrac 5", "pT B / pT jet", 60, 0, 1.2))); 
    ANA_CHECK (book (TH1F ("bPtFrac 6", "pT B / pT jet", 60, 0, 1.2))); 
    ANA_CHECK (book (TH1F ("bPtFrac 7", "pT B / pT jet", 60, 0, 1.2))); 
    ANA_CHECK (book (TH1F ("bPtFrac 8", "pT B / pT jet", 60, 0, 1.2))); 
    ANA_CHECK (book (TH1F ("bPtFrac 9", "pT B / pT jet", 60, 0, 1.2))); 
    ANA_CHECK (book (TH1F ("bPtFrac 10", "pT B / pT jet", 60, 0, 1.2))); 

    // HF reweight functions
    // SH2210->Sh221 B fragmentation function
    //  TFile* ratHistfile=TFile::Open("ratiohists_Sh2210.root","READ");
    // SH2210->Sh221 B fragmentation function - after Branching fraction reweight
    TFile* ratHistfile=TFile::Open("ratiohists_Sh2210bfrw.root","READ");
    ratHistfile->ls();
    m_ratHist=(TH1*)ratHistfile->Get("bPtFrac");
  
  }

  if (m_doVjets) {
    ANA_CHECK (book (TH1F ("pTV", "pTV", 100,0.,2000))); 
    ANA_CHECK (book (TH1F ("HT", "HT", 100,0.,2000))); 
    ANA_CHECK (book (TH2F ("pTV_HT", "pTV vs HT", 100,0.,1000.,100,0.,1000.))); 
    ANA_CHECK (book (TH2F ("lg10pTV_lg10HT", "log10(pTV) vs log10(HT)",100,-1.,4.,100,-1.,4.)));
  }

  // total nominal weight for cross checks
  ANA_CHECK (book (TH1F ("nomw", "sum nominal weights", 1, 0, 1))); 
  ANA_CHECK (book (TH1F ("nomw0", "sum nominal weights", 1, 0, 1)));

  // weight values themselves 
  ANA_CHECK (book (TH1F ("weights", "weight", 2000, -1000, 1000))); 
  ANA_CHECK (book (TH1F ("weights_zoom", "weight", 200, -0, 10))); 




  return StatusCode::SUCCESS;
}





StatusCode TruthxAODAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  ANA_MSG_INFO ("in execute");

  // steering
  bool debug=true;
  bool doleptons=true;
  bool dojets=true;
  bool doHardScatter = false; //(m_doVjets) ? true : false;

  // reweight 2.2.10 to 2.2.1 HF branching fractions
  bool applyBfRw=false; 
  static const float BF_sherpa2210_to_221[11]={0.673317,0.677142,0.927389,1.27163,3.8068,3.2978,3.37642,2.93738,0,0,0};
  
  // reweight 2.2.10 to 2.2.1 fragmentation function
  bool applyBFrag=false; 

  // retrieve the eventInfo object from the event store
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));

  // print out run and event number from retrieved object
  //ANA_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());

  std::vector<float> weights = eventInfo->mcEventWeights();

  //
  double weight=weights[0];
  double weight0=weight;

  //if (weight>weightMax) weight=weightMax;

  // fill Nominal weights
  hist ("nomw")->Fill (0.5, weight); 
  hist ("nomw0")->Fill (0.5, weight0); 

  hist ("weights")->Fill (weight, 1.0); 
  hist ("weights_zoom")->Fill (weight, 1.0); 

  // leptons
  TLorentzVector mu0(0,0,0,0);        
  TLorentzVector mu1(0,0,0,0);
  int nmu(0);  // all loose muons        
  int nZmu(0); // all Z muons        
  int imu0(-1);        
  int imu1(-1);        
  int ind(-1);
  if (doleptons) {
    // Classifier details:        
    // https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/MCTruthClassifier/MCTruthClassifier/MCTruthClassifierDefs.h 

    const xAOD::TruthParticleContainer* truthMuons = 0;        
    ANA_CHECK (evtStore()->retrieve (truthMuons, "TruthMuons"));
    for (const xAOD::TruthParticle* part : *truthMuons) { 	 
      ind++; 	 
      int lepton_type   = ( part )->auxdata< unsigned int >("classifierParticleType"); 	 
      if (debug) 
	std::cout << "muon type " << lepton_type << " pt " << part->pt()/1000. << std::endl; 	 
      //if (lepton_type != 6) continue; // isolated 	 
      if (part->pt()<7000 || fabs(part->eta())>2.7) continue; 	 
      nmu++; 	 
      if (nZmu>2) continue; 	 
      unsigned int ori = part->auxdata< unsigned int >("classifierParticleOrigin"); 	 
      if (debug) 
	std::cout << "origin " << ori << std::endl; 	 
      //if (ori != 13) continue; // Z boson 	 
      //if (ori != 14) continue; // Higgs boson 	 
      if (fabs(part->eta())>2.5 ) continue; 	 
      nZmu++; 	 
      if (imu0<0) { 	   
	mu0.SetPtEtaPhiM(part->pt(),part->eta(),part->phi(),0.0); 	   
	imu0=ind; 
      } else if (imu1<0) { 	   
	mu1.SetPtEtaPhiM(part->pt(),part->eta(),part->phi(),0.0); 	   
	imu1=ind; 	 
      }        
    } // end of muon loop

  }


  // jets
  float weightBf=1.0;
  float weightBFrag=1.0;
  int ncentjets=0;
  int nforwjets=0;
  int nbjets=0;       
  int njets=0;
  float HT=0;
  int indhighestnonbjet=-1;
  float pthighestnonbjet=0;  
  std::vector<TLorentzVector> jets;
  std::vector<TLorentzVector> bjets;
  std::string jetContainerName("");
  if (dojets) {
    if (evtStore()->contains<xAOD::JetContainer>("AntiKt4TruthWZJets")) {
      jetContainerName="AntiKt4TruthWZJets";
    } else if (evtStore()->contains<xAOD::JetContainer>("AntiKt4TruthDressedWZJets")) {
      jetContainerName="AntiKt4TruthDressedWZJets";
      std::cout << " has not " << std::endl; 
    }

    const xAOD::JetContainer* truthJets = 0;
    ANA_CHECK (evtStore()->retrieve (truthJets, jetContainerName));
    int indjet=-1;
    for (const xAOD::Jet* jet : *truthJets) {
      float eta=jet->eta();
      float pt=jet->pt()/1000;   
      //
      int label(1000);
      jet->getAttribute<int>("HadronConeExclTruthLabelID",label);
      // calculating HT (no eta cuts)
      if (pt>20.0) {
	// Remove hadronic taus e.g. from Ztautau, Wtaunu decays     
	if (label==15) continue;
	HT+=pt;
      }

      // standard analysis
      if(pt < 20 || fabs(eta)>4.5) continue;
      // OR with all Z leptons to be extra safe
      TLorentzVector p4 = jet->p4();
      //if (p4.DeltaR(mu0) < 0.4 || p4.DeltaR(mu1) < 0.4) continue;
      //if (p4.DeltaR(el0) < 0.4 || p4.DeltaR(el1) < 0.4) continue;

      // forward jets
      if(fabs(eta)>2.5 && pt<30 ) continue;  
      indjet++;    
      jets.push_back(p4); // all jets
      if(fabs(eta)<2.5) {
	ncentjets++;
	//std::cout << " got a jet " << pt << " " << eta << " " << label << std::endl;
	if (label==5) {
	  bjets.push_back(p4);
	  nbjets++;
	  //
	  // look at hadrons
	  std::vector<const xAOD::TruthParticle*> BHadrons;
	  const std::string labelB = "ConeExclBHadronsFinal"; // = "GhostBHadronsFinal"; // alternative association scheme
          jet->getAssociatedObjects<xAOD::TruthParticle>(labelB, BHadrons);
	  // 
	  if (debug)
	    std::cout << "Number of B hadrons " << BHadrons.size() << std::endl;
	  //
	  for (long unsigned int ihad=0 ; ihad< BHadrons.size() ; ihad++){
	    // 	    
	    int pdg=BHadrons[ihad]->absPdgId();
	    //std::cout << " bhadron " << ihad << " " << pdg << std::endl;
	    float wbf=1.0;
	    float bPtFrac=BHadrons[ihad]->pt()/jet->pt();
	    if (pdg==511) {	      
	      wbf = (applyBfRw) ? BF_sherpa2210_to_221[0] : 1.0;
	      //std::cout << " wbf " << wbf << std::endl;
	      hist ("bDecays")->Fill (0.5, weight*wbf); // B0
	      hist ("bPtFrac 0")->Fill (bPtFrac, weight); 
	    } else if (pdg==521) {
	      wbf = (applyBfRw) ? BF_sherpa2210_to_221[1] : 1.0;
	      hist ("bDecays")->Fill (1.5, weight*wbf); // B+
	      hist ("bPtFrac 1")->Fill (bPtFrac, weight); 
	    } else if (pdg==531) {
	      wbf = (applyBfRw) ? BF_sherpa2210_to_221[2] : 1.0;
	      hist ("bDecays")->Fill (2.5, weight*wbf); // B_s0
	      hist ("bPtFrac 2")->Fill (bPtFrac, weight); 
	    } else if (pdg==541) {
	      wbf = (applyBfRw) ? BF_sherpa2210_to_221[3] : 1.0;
	      hist ("bDecays")->Fill (3.5, weight*wbf); // B_c+
	      hist ("bPtFrac 3")->Fill (bPtFrac, weight); 
	    } else if (pdg==5122) {
	      wbf = (applyBfRw) ? BF_sherpa2210_to_221[4] : 1.0;
	      hist ("bDecays")->Fill (4.5, weight*wbf); // Lam_b0
	      hist ("bPtFrac 4")->Fill (bPtFrac, weight); 
	    } else if (pdg==5132) {
	      wbf = (applyBfRw) ? BF_sherpa2210_to_221[5] : 1.0;
	      hist ("bDecays")->Fill (5.5, weight*wbf); // Sig_b-
	      hist ("bPtFrac 5")->Fill (bPtFrac, weight); 
	    } else if (pdg==5232) {
	      wbf = (applyBfRw) ? BF_sherpa2210_to_221[6] : 1.0;
	      hist ("bDecays")->Fill (6.5, weight*wbf); // Lam_b0
	      hist ("bPtFrac 6")->Fill (bPtFrac, weight); 
	    } else if (pdg==5332) {
	      wbf = (applyBfRw) ? BF_sherpa2210_to_221[7] : 1.0;
	      hist ("bDecays")->Fill (7.5, weight*wbf); // Om_b-
	      hist ("bPtFrac 7")->Fill (bPtFrac, weight); 
	    } else if (pdg==5112) {
	      wbf = (applyBfRw) ? BF_sherpa2210_to_221[8] : 1.0;
	      hist ("bDecays")->Fill (8.5, weight*wbf); // Sig_b-
	      hist ("bPtFrac 8")->Fill (bPtFrac, weight); 
	    } else if (pdg==5212) {
	      wbf = (applyBfRw) ? BF_sherpa2210_to_221[9] : 1.0;
	      hist ("bDecays")->Fill (9.5, weight*wbf); // Sig_b0
	      hist ("bPtFrac 9")->Fill (bPtFrac, weight); 
	    } else if (pdg==5222) {
	      wbf = (applyBfRw) ? BF_sherpa2210_to_221[10] : 1.0;
	      hist ("bDecays")->Fill (10.5, weight*wbf); // Sig_b+
	      hist ("bPtFrac 10")->Fill (bPtFrac, weight); 
	    }
	    weightBf*=wbf;
	    //
	    float wbfrag=1.0;
	    if (applyBFrag) {
	      int fragBbin=m_ratHist->FindBin(bPtFrac);
	      wbfrag=m_ratHist->GetBinContent(fragBbin);
	    }
	    weightBFrag*=wbfrag;
	    // fragmentation function with all the reweights
	    hist ("bPtFrac")->Fill (bPtFrac, weight*wbf*wbfrag); 

	  }
	} else {
	  if (pt>pthighestnonbjet) {
	    pthighestnonbjet=pt;
	    indhighestnonbjet=indjet;
	  }
	}
      } else 
	nforwjets++;
    } // truth jets
    njets=jets.size();       
    if (debug) {
      std::cout << "njets " << njets << std::endl;
      std::cout << "nbjets " << nbjets << std::endl;
    }
  } // dojets
  
  float pTV=0;
  if (doHardScatter) {
    // replicate ComputeSherpapTV()  https://gitlab.cern.ch/CxAODFramework/CxAODMaker/-/blob/master/Root/TruthProcessor.cxx#L897
    TLorentzVector final_V;
    const xAOD::TruthParticleContainer* hardScatter = 0;        
    ANA_CHECK (evtStore()->retrieve (hardScatter, "HardScatterParticles"));
    for (const xAOD::TruthParticle* particle : *hardScatter) { 
      if (particle->status() == 3) {    
	// Getting truth boson information (for Sherpa)       
	if (!particle->isLepton()) continue; // Only interested in the leptonic decays!       
	final_V += particle->p4();
      }
    }
    pTV=final_V.Pt()/1000.;
    if (debug)
      std::cout << "pTV: " << pTV << std::endl;
    if (debug)
      std::cout << "HT: " << HT << std::endl;
    //
    hist ("pTV")->Fill (pTV, weight); 
    hist ("HT")->Fill (HT, weight); 
    //hist ("pTV_HT")->Fill (pTV,HT, weight); 
    //hist ("lg10pTV_lg10HT")->Fill (pTV,HT, weight); 

  }

  // Event selections
  // now we require btags and one of the bjets to be above 45 GeV

  bool selbjets=true;
  if (debug)
    std::cout << " nbjets " << nbjets << std::endl;
  if (nbjets!=2) selbjets=false; // 2 tags
  if (debug && nbjets >= 1)
    std::cout << " b0 " << bjets.at(0).Pt() << std::endl;
  if (debug && nbjets ==2)
    std::cout << " b1 " << bjets.at(1).Pt() << std::endl;
  
  if (m_doVZ) {
    // cut was not applied in June 2021 studies
    //if (bjets.at(0).Pt()<45000&&bjets.at(1).Pt()<45000) selbjets=false;

    if (!selbjets) return StatusCode::SUCCESS;
  }


  // Make particles
  // build h(from b jets) and Z from leptons
  TLorentzVector h;
  TLorentzVector Z;

  if (nbjets==2) {
    //	 std::cout << "bjets size " << bjets.size() << std::endl;
    h+=bjets.at(0);
    h+=bjets.at(1);
    h*=1e-3; // convert to GeV
  }


  // reweights
  if (applyBfRw) {
    //std::cout << " applying branching fraction reweight " << weightBf << std::endl;
    weight*=weightBf;
  }
  if (applyBFrag) {
    //std::cout << " applying B fragmentation reweight " << weightBFrag << std::endl;
    weight*=weightBFrag;
  }

  // Higgs mass converted to GeV already
  if (debug && nbjets==2)
    std::cout << " h mass " << h.M() << std::endl;
  //       if (h.M()<110 || h.M()>160) continue;
  
  // sort jets
  /*
    std::sort(jets.begin(), jets.end(), sort_pt);
    float mjj=-1;
    //float dRjj=-1;
    if (jets.size()>1) {
    TLorentzVector mjjvec(jets.at(0));
	 mjjvec+=jets.at(1);
	 mjj=mjjvec.M();      
	 //dRjj=jets.at(0).DeltaR(jets.at(1));
	 } 
  */
  TString jet_str = (njets==2) ? "2jet" : "3pjet";
  TString tag_str = (nbjets==1) ? "1tag" : "2tag";


  //fillHistos(mllhists[tag_str+jet_str],Z.M(),nomw);

  // 1. Fill pTV and mBB histograms after cuts
  //if (!mllcut) continue;
  //fillHistos(ptvhists[tag_str+jet_str],Z.Pt(),nomw);
  //fillHistos(ptv3hists[tag_str+jet_str],Zstatus3.Pt(),nomw);
   
  if (m_doVZ) {
    if (nbjets==2) {
      hist ("mbb")->Fill (h.M(), weight); 
    }
  }
       
  if (m_doVjets) {

  }


  return StatusCode::SUCCESS;
}



StatusCode TruthxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  return StatusCode::SUCCESS;
}
