#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )
# Sherpa VZllbb studies
# Sherpa 2.2.10 validation sample
inputFilePath_Sh2210 ='/eos/user/t/thompson/ATLAS/TRUTH1_diboson/mc15_13TeV.950143.Sh_2210_ZZ_llbb_EnhMaxPTV_valid.deriv.DAOD_TRUTH1.e8253_p4527/'
#ROOT.SH.ScanDir().filePattern( 'DAOD_TRUTH1.25765401._000065.pool.root.1' ).scan( sh, inputFilePath_Sh2210 )
#ROOT.SH.ScanDir().filePattern( '*.pool.root*' ).scan( sh, inputFilePath_Sh2210 )
# Sherpa 2.2.1 sample
inputFilePath_Sh221='/eos/user/t/thompson/ATLAS/TRUTH1_diboson/mc15_13TeV.345044.Sherpa_221_NNPDF30NNLO_ZbbZll.deriv.DAOD_TRUTH1.e6470_e5984_p4527/'
#ROOT.SH.ScanDir().filePattern( '*.pool.root*' ).scan( sh, inputFilePath_Sh221 )
# Powheg
inputFilePath_Powheg='/eos/user/t/thompson/ATLAS/TRUTH1_diboson/mc15_13TeV.361610.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZqqll_mqq20mll20.deriv.DAOD_TRUTH1.e4711_p4527/'
#ROOT.SH.ScanDir().filePattern( '*.pool.root*' ).scan( sh, inputFilePath_Powheg )

# Sherpa V+jets
inputFilePath_Wmu_maxHtPtv500_1000='/eos/user/t/thompson/ATLAS/TRUTH3_vjets/mc15_13TeV.364168.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV500_1000.deriv.DAOD_TRUTH3.e5340_e5984_p4161/'
#ROOT.SH.ScanDir().filePattern( '*.pool.root*' ).scan( sh, inputFilePath_Wmu_maxHtPtv500_1000)

# Higgs lifetime
inputFilePath_hmumu='/eos/user/t/thompson/ATLAS/TRUTH1_hmumu/group.phys-higgs.345097.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_mumu.evgen.EVNT.e5732.002.TRUTH1_EXT0/'
ROOT.SH.ScanDir().filePattern( '*.pool.root*' ).scan( sh, inputFilePath_hmumu)

sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
job.options().setDouble( ROOT.EL.Job.optMaxEvents, 500 )
##job.options().setString( ROOT.EL.Job.optSubmitDirMode, 'unique-link')

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'TruthxAODAnalysis', 'AnalysisAlg' )

# later on we'll add some configuration options for our algorithm that go here

# Add our algorithm to the job
job.algsAdd( alg )

# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
driver.submit( job, options.submission_dir )
