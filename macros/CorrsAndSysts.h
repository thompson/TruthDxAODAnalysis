// duplicating CorrsAndSysts functions for now
#include <memory>
#include <TF1.h>
std::shared_ptr<TF1> m_f_SysVVMbbME_ZZllqq_2j;
std::shared_ptr<TF1> m_f_SysVVMbbME_ZZllqq_3j;
std::shared_ptr<TF1> m_f_SysVVPTVME_ZZllqq_2j;
std::shared_ptr<TF1> m_f_SysVVPTVME_ZZllqq_3j;
std::shared_ptr<TF1> m_f_SysVVPTVPSUE_ZZllqq_2j;
std::shared_ptr<TF1> m_f_SysVVPTVPSUE_ZZllqq_3j;

void setCorrsAndSysts() {

  m_f_SysVVMbbME_ZZllqq_2j =
    std::make_shared<TF1>("mBB__ZZqqll_PS_Sherpa_fit",
			  "[0]+[1]*tanh( (x/1e3-[2])/[3] )", 25e3, 500e3);
  
  m_f_SysVVMbbME_ZZllqq_2j->SetParameter(0, 0.18724);
  m_f_SysVVMbbME_ZZllqq_2j->SetParameter(1, 3.20219e-01);
  m_f_SysVVMbbME_ZZllqq_2j->SetParameter(2, 9.38599e+01);
  m_f_SysVVMbbME_ZZllqq_2j->SetParameter(3, 1.45278e+01);

  m_f_SysVVMbbME_ZZllqq_3j = 
    std::make_shared<TF1>(
			  "mBB__ZZqqll_PS_Sherpa_fit_3J",
			  " x<250e3 ? [0]+[1]*x/1e3+[2]*x*x/1e6+[3]*x*x*x/1e9 "
			  ":[0]+[1]*250+[2]*250*250+[3]*250*250*250",
			  25e3, 500e3);
  m_f_SysVVMbbME_ZZllqq_3j->SetParameter(0, 2.42069e-01 - 1);
  m_f_SysVVMbbME_ZZllqq_3j->SetParameter(1, 1.99211e-02);
  m_f_SysVVMbbME_ZZllqq_3j->SetParameter(2, -1.50742e-04);
  m_f_SysVVMbbME_ZZllqq_3j->SetParameter(3, 3.18587e-07);

  m_f_SysVVPTVME_ZZllqq_2j = 
    std::make_shared<TF1>(
			  "pTV__ZZqqll_PS__2jet_Sherpa_fit", "[0]+[1]/((x/1e3)+[3])+[2]*(x/1e3) -1",
			  25e3, 500e3);
  m_f_SysVVPTVME_ZZllqq_2j->SetParameter(0, -0.219101);
  m_f_SysVVPTVME_ZZllqq_2j->SetParameter(1, 862.682);
  m_f_SysVVPTVME_ZZllqq_2j->SetParameter(2, 0.00100774);
  m_f_SysVVPTVME_ZZllqq_2j->SetParameter(3, 684.347);

  m_f_SysVVPTVME_ZZllqq_3j = 
    std::make_shared<TF1>(
			  "pTV__ZZqqll_PS__3jet_Sherpa_fit", "[0]+[1]/((x/1e3)+[3])+[2]*(x/1e3) -1",
			  25e3, 500e3);
  m_f_SysVVPTVME_ZZllqq_3j->SetParameter(0, 0.0611405);
  m_f_SysVVPTVME_ZZllqq_3j->SetParameter(1, 792.836);
  m_f_SysVVPTVME_ZZllqq_3j->SetParameter(2, 0.000446438);
  m_f_SysVVPTVME_ZZllqq_3j->SetParameter(3, 795.418);

  m_f_SysVVPTVPSUE_ZZllqq_2j = 
    std::make_shared<TF1>(
			  "pTV__ZZqqll_PS__2jet_PS_fit", "[0]+[1]/((x/1e3)+[3])+[2]*(x/1e3) -1",
			  25e3, 500e3);
  m_f_SysVVPTVPSUE_ZZllqq_2j->SetParameter(0, 0.912408);
  m_f_SysVVPTVPSUE_ZZllqq_2j->SetParameter(1, 3.96622);
  m_f_SysVVPTVPSUE_ZZllqq_2j->SetParameter(2, 0.000266947);
  m_f_SysVVPTVPSUE_ZZllqq_2j->SetParameter(3, 10);

  m_f_SysVVPTVPSUE_ZZllqq_3j = 
    std::make_shared<TF1>(
			  "pTV__ZZqqll_PS__3jet_PS_fit", "[0]+[1]/((x/1e3)+[3])+[2]*(x/1e3) -1",
			  25e3, 500e3);
  m_f_SysVVPTVPSUE_ZZllqq_3j->SetParameter(0, 0.938234);
  m_f_SysVVPTVPSUE_ZZllqq_3j->SetParameter(1, 1.93089);
  m_f_SysVVPTVPSUE_ZZllqq_3j->SetParameter(2, 0.000351818);
  m_f_SysVVPTVPSUE_ZZllqq_3j->SetParameter(3, 10);

}


void getCorrsAndSystsHists(TH1* hsysup, TH1* hsysdo, std::shared_ptr<TF1> errFunc, float scalefac) {
  //
  // input - histograms to be scaled by error function (output)
  //         error function
  //         scale factor for x-axis
  // 

  for (int i=1;  i<=hsysup->GetNbinsX(); i++ ) {
    float cen=hsysup->GetBinCenter(i);
    float binc=hsysup->GetBinContent(i);
    float err= errFunc->Eval(cen*scalefac);
    hsysup->SetBinContent(i,binc*(1+err));
    hsysdo->SetBinContent(i,binc*(1-err));
  }

}


