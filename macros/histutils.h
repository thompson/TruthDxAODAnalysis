//
typedef TString Str;
typedef std::vector<TH1*> HistV;
typedef std::vector<TString> StrV;
typedef std::map<TString,HistV> HistMap;
typedef std::vector<double> NumV;
void fatal(Str msg) { printf("\n\nFATAL\n  %s\n\n",msg.Data()); abort(); }

// methods to access hists
TFile *openFile(Str fn);
TH1 *getHist(TFile *f, Str hn, int rebin=1);
TH1 *getHist(Str fn, Str hn, int rebin=1) { return getHist(openFile(fn),hn,rebin); }

// methods to draw stuff, including legends
void drawLine(double x1, double y1, double x2, double y2) {
  static TLine *line = new TLine(); line->DrawLine(x1,y1,x2,y2);
}
TH1* drawHist(TH1 *h, Str opt, int col=kBlack, int ls=1, int lw=2) {
  h->SetStats(0); h->SetLineColor(col); h->SetMarkerColor(col); h->SetLineWidth(lw); h->SetLineStyle(ls); h->Draw(opt); return h;
}
void drawText(double x, double y, Str txt, int col=kBlack) {
  static TLatex *t = new TLatex(); t->SetNDC(); t->SetTextFont(42); t->SetTextColor(col); t->DrawLatex(x,y,txt);
}

void addLegendEntry(TLegend* leg, TH1* hist, TString label, Option_t* option = "lpf" ) {
  //
  // add label to legend if not already added
  //
  bool hasEntry=false;
  auto primitives = leg->GetListOfPrimitives();
  for (auto primitiveObj :  *primitives){
    auto primitive = (TLegendEntry*)primitiveObj;
    if (primitive->GetLabel()==label) {
      hasEntry=true;
      break;
    }
  }
  if (!hasEntry)
    leg->AddEntry(hist ,label, option);

}




// methods to calculate errors
double addInQuad(const NumV &v, double nom=0) {
  double V=0; for (auto var:v) V+=pow(var-nom,2); return sqrt(V);
}
double addInQuadRel(const NumV &v, double nom) {
  double V=0; for (auto var:v) V+=pow((var-nom)/nom,2); return sqrt(V);
}
double envelopeRel(const NumV &vec, double n, bool ignoreZero=true) {
  double max=0;
  for (auto v:vec) {
    double u=std::abs(v-n)/n; if (u>max&&!(ignoreZero&&v==0)) max=u;
  } return max;
}
// assumes uncorrelated systematic sources (Hessian)
double getTotUnc(HistV sysVar, int bin) {
  double nom=sysVar[0]->GetBinContent(bin), V=0;
  for (int i=1;i<sysVar.size();++i)
    V+=pow((sysVar[i]->GetBinContent(bin)-nom)/nom,2);
  return sqrt(V);
}
// Some additions for acceptance uncertainties
/// acceptance for cut
double addInQuadAcc(const NumV &v, double nom, const NumV &vcut, double nomcut) {
  double V=0; 
  double nomacc=nomcut/nom;
  //cout << "nomacc " << nomacc << endl;
  for (int j(0); j<v.size(); ++j) {
    double varacc=vcut.at(j)/v.at(j);
    double err=varacc/nomacc;
    V+=pow((err-1),2); 
  }
  return sqrt(V);
}
double envelopeAcc(const NumV &vec, double n, const NumV &veccut, double ncut, bool ignoreZero=true) {
  double max=0;
  double nomacc=ncut/n;
  for (int j(0); j<vec.size(); ++j) {
    double varacc=veccut.at(j)/vec.at(j);
    double err=varacc/nomacc;
    double u=std::abs(varacc-nomacc)/nomacc; 
    //cout << j << " " << nomacc << " " << varacc << " " << u << " " << max << endl;
    if (u>max&&!(ignoreZero&&vec.at(j)==0)) max=u;
  }
  return max;
}


void drawRatio(TH1 *h, HistV pdf, HistV aS, HistV qcd, bool qcdEnv=true);
TH1* drawRatioAxis(TH1 *nom, float ymin=0.6, float ymax=1.4);
void drawRatioBands(TH1 *h, HistV qcd, bool qcdEnv=true, bool isNnlops=true);
void drawRatioAcc(TH1 *h, HistV pdf, HistV aS, HistV qcd, bool qcdEnv=true);
void drawRatioAccQCD(TH1 *h, HistV pdf, HistV aS, HistV qcd, bool print=false); // for 2NP QCD
void drawRatioShower(TH1 *nom, HistV sys, bool error=false);
void addInQuad(TH1 *tot, TH1 *unc);


double hxswg(Str p) {
  //https://twiki.cern.ch/twiki/bin/view/LHCPhysics/CERNYellowReportPageAt13TeV
  if (p=="ggF") return 4.858E+01;
  if (p=="VBF") return 3.782E+00;
  if (p=="WpH") return 8.400E-01;
  if (p=="WmH") return 5.328E-01;
  if (p=="ZH" ) return 8.839E-01-1.227E-01; // sig(qq->ZH) = sig(pp->ZH) - sig(gg->ZH)
  return 0.0;
}
// print relative deviation in percent between var and nom
char *per(double relDev) { return relDev?Form("%.2f%%",relDev*100):Form("N/A"); }
char *per(double var, double nom) { return per((var-nom)/nom); }
//


/////////////////////////////////////////////////////////////////////////
TH1* drawRatioAxis(TH1 *nom, float ymin, float ymax) {
  TH1D *axis = (TH1D*)nom->Clone();
  axis->GetYaxis()->SetRangeUser(ymin,ymax);
  for (int bin=1;bin<=axis->GetNbinsX();++bin) {
    double y=axis->GetBinContent(bin);
    axis->SetBinError(bin,std::abs(y)<1e-5?0:axis->GetBinError(bin)/y); axis->SetBinContent(bin,1.0);
  }
  axis->SetYTitle("Impact of systematic variation relative to nominal");
  axis->Draw(); return axis;
}

void drawRatio(TH1 *nom, HistV pdf, HistV aS, HistV qcd, bool qcdEnv) {
  TH1* axis=drawRatioAxis(nom);
  //return;
  TH1D *pdfUnc = (TH1D*)nom->Clone(); pdfUnc->Reset();
  for (int bin=1;bin<=pdfUnc->GetNbinsX();++bin) pdfUnc->SetBinContent(bin,1.0);
  TH1D *qcdUnc = (TH1D*)pdfUnc->Clone();
  for (auto h:pdf) { h->Divide(nom); addInQuad(pdfUnc,h); }
  for (auto h:qcd) { h->Divide(nom); addInQuad(qcdUnc,h); }
  qcdUnc->SetFillColor(kRed-9);
  if (qcdEnv) {
    for (int bin=1;bin<=qcdUnc->GetNbinsX();++bin) {
      double max=0;
      for (auto h:qcd)
	if (std::abs(h->GetBinContent(bin)-1)>max)
	  max=std::abs(h->GetBinContent(bin)-1);
      qcdUnc->SetBinError(bin,max==1?0:max);
    } 
  }

  // copied from elsewhere
  if (qcdEnv) {
    qcdUnc->SetFillColor(kRed-9); 
    qcdUnc->Draw("e2 same");
  } else {
    for (auto h:qcd) {
      h->Divide(nom);
      h->Draw("same hist");
    }
  }

  pdfUnc->SetFillColor(kGray);
  pdfUnc->Draw("e2 same");
  for (auto h:aS) { TH1* h2=(TH1*)h->Clone(); h2->Divide(nom); drawHist(h2,"hist same",kBlue); }
  axis->Draw("same");


  
}

void drawRatioBands(TH1 *nom, HistV qcd, bool qcdEnv, bool isNnlops) {
  /*
 0:  nnlops-nnloDn-pwgDnDn  
 1:  nnlops-nnloDn-pwgDnNom  
 2:  nnlops-nnloDn-pwgDnUp  
 3:  nnlops-nnloDn-pwgNomDn  
 4:  nnlops-nnloDn-pwgNomNom    - "NNLO"
 5:  nnlops-nnloDn-pwgNomUp  
 6:  nnlops-nnloDn-pwgUpDn  
 7:  nnlops-nnloDn-pwgUpNom  
 8:  nnlops-nnloDn-pwgUpUp  
-------------------
 9:  nnlops-nnloNom-pwgDnDn  - "NLO"
10:  nnlops-nnloNom-pwgDnNom  
11:  nnlops-nnloNom-pwgDnUp  
12:  nnlops-nnloNom-pwgNomDn  
13:  nnlops-nnloNom-pwgNomUp  
14:  nnlops-nnloNom-pwgUpDn  
15:  nnlops-nnloNom-pwgUpNom  
16:  nnlops-nnloNom-pwgUpUp  
--------------------
17:  nnlops-nnloUp-pwgDnDn  
18:  nnlops-nnloUp-pwgDnNom  
19:  nnlops-nnloUp-pwgDnUp  
20:  nnlops-nnloUp-pwgNomDn  
21:  nnlops-nnloUp-pwgNomNom  <-- nominal
22:  nnlops-nnloUp-pwgNomUp 
23:  nnlops-nnloUp-pwgUpDn  
24:  nnlops-nnloUp-pwgUpNom  
25:  nnlops-nnloUp-pwgUpUp
  */

  TH1* axis=drawRatioAxis(nom,0.2,1.8);
  //return;
  TH1D *qcdUnc = (TH1D*)nom->Clone(); qcdUnc->Reset();
  for (int bin=1;bin<=qcdUnc->GetNbinsX();++bin) qcdUnc->SetBinContent(bin,1.0);
  for (int bin=1;bin<=qcdUnc->GetNbinsX();++bin) qcdUnc->SetBinError(bin,0.0);
  for (auto h:qcd) { h->Divide(nom); addInQuad(qcdUnc,h); }
  qcdUnc->SetFillColor(kRed-9);

  if (qcdEnv) {
    for (int bin=1;bin<=qcdUnc->GetNbinsX();++bin) {
      double max=0;
      for (auto h:qcd)
      	if (std::abs(h->GetBinContent(bin)-1)>max)
      	  max=std::abs(h->GetBinContent(bin)-1);
      double error = (max==1) ? 0 : max;
      cout << bin  << " " << error << endl;
      qcdUnc->SetBinError(bin,error);
      cout << bin << " " << qcdUnc->GetBinContent(bin) << " " << qcdUnc->GetBinError(bin) << endl; 
    }// qcdFunc bins
  }// qcdEnv

  if (qcdEnv) {
    qcdUnc->SetFillColor(kRed-9); 
    qcdUnc->Draw("e2 same");
  } else {
    for (auto h:qcd) {
      h->Divide(nom);
      h->Draw("same hist");
    }
  }
  if (isNnlops) {
    for(int i(0) ; i<1;++i) {
      qcd.at(i)->SetLineStyle(i+1);
      qcd.at(i)->Draw("same hist");
      //    qcd.at(1)->Draw("same hist");
      // qcd.at(2)->Draw("same hist");
    }
  } else {
    // NLO position 1 which is element 9
    for(int i(0) ; i<1;++i) {
      qcd.at(i)->SetLineStyle(i+1);
      qcd.at(i)->Draw("same hist");
    }
  }

  
  axis->Draw("same");

}

//
void drawRatioAcc(TH1 *nom, HistV pdf, HistV aS, HistV qcd, bool qcdEnv) {
  TH1* axis=drawRatioAxis(nom);
  //return;
  TH1D *pdfUnc = (TH1D*)nom->Clone(); pdfUnc->Reset();
  for (int bin=1;bin<=pdfUnc->GetNbinsX();++bin) pdfUnc->SetBinContent(bin,1.0);
  TH1D *qcdUnc = (TH1D*)pdfUnc->Clone();
  for (auto h:pdf) {TH1* h2=(TH1*)h->Clone(); h2->Divide(nom); addInQuad(pdfUnc,h2);}

  //for (auto h:qcd) { h->Divide(nom); addInQuad(qcdUnc,h); }
  // QCD envelope
  if (qcdEnv) {
    // first divide errors by nominal
    for (auto h:qcd) h->Divide(nom);
    // loop over bins and set error to max difference to 1 
    for (int bin=1;bin<=qcdUnc->GetNbinsX();++bin) {
      double max=0;
      int j(0);
      float nomc=nom->GetBinContent(bin);
      for (auto h:qcd) {
	//cout << " qcd error " << j << " max " << max << " content " << h->GetBinContent(bin) << " diff " << std::abs(h->GetBinContent(bin)-1)  << endl;
	if (std::abs(h->GetBinContent(bin)-1)>max)
	  max=std::abs(h->GetBinContent(bin)-1);
      }
      //cout << " setting error to " << (max==1?0:max) << endl;
      qcdUnc->SetBinError(bin,max==1?0:max);      
    } 
  }
  
  if (qcdEnv) {
    qcdUnc->SetFillColor(kRed-9); 
    qcdUnc->Draw("e2 same");
  } else {
    for (auto h:qcd) {
      h->Divide(nom);
      h->Draw("same hist");
    }
  }

  pdfUnc->SetFillColor(kGray);
  pdfUnc->Draw("e2 same");
  for (auto h:aS) { TH1* h2=(TH1*)h->Clone(); h2->Divide(nom); drawHist(h2,"hist same",kBlue); }
  axis->Draw("same");
}

// ratio of uncertainties for 2 point QCD
void drawRatioAccQCD(TH1 *nom, HistV pdf, HistV aS, HistV qcd, bool print) {
  TH1* axis=drawRatioAxis(nom);
  //return;
  TH1D *pdfUnc = (TH1D*)nom->Clone(); pdfUnc->Reset();
  for (int bin=1;bin<=pdfUnc->GetNbinsX();++bin) pdfUnc->SetBinContent(bin,1.0);
  TH1D *qcdUnc1 = (TH1D*)qcd.at(0)->Clone();
  TH1D *qcdUnc2 = (TH1D*)qcd.at(1)->Clone();
  qcdUnc1->Divide(nom);
  qcdUnc2->Divide(nom); 
  for (auto h:pdf) {TH1* h2=(TH1*)h->Clone(); h2->Divide(nom); addInQuad(pdfUnc,h2);}

  if (print) {
    for (auto h:qcd) {
      h->Divide(nom);
      cout << " qcd error " << endl;
      for (int bin=1;bin<=h->GetNbinsX();++bin) {
	cout << (h->GetBinContent(bin)-1)*100 << ",";
      }
    }
  }
  cout << endl;
  pdfUnc->SetFillColor(kGray);
  pdfUnc->Draw("e2 same");
  for (auto h:aS) { TH1* h2=(TH1*)h->Clone(); h2->Divide(nom); drawHist(h2,"hist same",kBlue); }

  qcdUnc1->SetLineColor(kRed);
  qcdUnc1->SetLineStyle(2);
  qcdUnc1->Draw("same hist");
  qcdUnc2->SetLineColor(kMagenta);
  qcdUnc2->SetLineStyle(3);
  qcdUnc2->Draw("same hist");  

  axis->Draw("same");
}

////
void drawRatioShower(TH1 *nom, HistV sys, bool error) {
  TH1* nomclone=(TH1*)nom->Clone();
  nom->Divide(nom);
  TH1* axis=drawRatioAxis(nom);
  //return;
  HistV sysclone;
  // make a clone for errors
  if (error) {
    for (auto h:sys) { TH1* clone=(TH1*)h->Clone(); sysclone.push_back(clone);}
  }
  for (auto h:sys) { h->Divide(nomclone); }
  // set the errors on the sys to be the errors from the original
  if (error) {
    for (int i(0); i<sys.size(); i++) {
      for ( int bin(0) ; bin<sysclone.at(i)->GetNbinsX(); ++bin) {
	float error = (sysclone.at(i)->GetBinContent(bin) == 0) ? 0 : sysclone.at(i)->GetBinError(bin)/sysclone.at(i)->GetBinContent(bin);
	sys.at(i)->SetBinError(bin,error);
      } // hist bins
    } // histograms
  }
  for (auto h:sys) { drawHist(h,"hist same",h->GetLineColor(),h->GetLineStyle()); }
  axis->Draw("same");
}


TFile *openFile(Str fn) {
  TFile *f=TFile::Open(fn);
  if (f==nullptr||f->IsZombie()) fatal("Cannot open "+fn);
  return f;
}

TH1 *getHist(TFile *f, Str hn, int rebin) {  
  f->cd();
  //gDirectory->ls();
  TH1 *h = (TH1*)f->Get(hn);
 //  TH1 *horig = (TH1*)f->Get(hn);
  // TH1 *h = (TH1*)horig->Clone();
  if (h==nullptr) fatal("Cannot access histo "+hn+" in file "+f->GetName());
  h->Rebin(rebin); return h;
}

// add uncertainty source unc to total
// unc is relative to 1
void addInQuad(TH1 *tot, TH1 *uncVar) {
  for (int bin=1;bin<=tot->GetNbinsX();++bin) {
    double err1 = tot->GetBinError(bin);
    double err2 = uncVar->GetBinContent(bin)-1;
    if (err2!=-1)
      tot->SetBinError(bin,sqrt(err1*err1+err2*err2));
  }
}
