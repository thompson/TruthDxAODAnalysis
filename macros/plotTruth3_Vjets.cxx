/*
 *  Macro to evaluate uncertainties on Dibosons
 *
 *  Details on offshell studies - 
 *   https://indico.cern.ch/event/800358/contributions/3428948/attachments/1843647/3023908/VVModellingHbbWorkshop.pdf
 *
 *  use plotset to set the histograms you want to draw
 *
 */
#include "histutils.h"  // useful functions for plotting histograms
#include "CorrsAndSysts.h" // error functions
//
void evaluateVVSysts(); 
void setCanvasAndPads();
void setupRatioHist(TH1* histdataR, TString ytitle="Ratio ");
void setInputOutput();
void setTitleRangeRebin(TH1* hist, TString ytitle);
void  mergeHistograms(StrV dsids, TString stem, StrV bins, StrV regions, StrV distribs, TString syst);
// histogram plotting functions
// using most common format
void plotHistograms(StrV dsids, TString stem, TString bin, TString region, TString var, TString syst, 
		    TLegend* leg, bool plotrat, bool shapeNormalised=false);
// version with histname as argument
void plotHistograms(TString histname, StrV dsids, TLegend* leg, TString legTitle, bool plotrat, 
		    bool doShape);

void makePlots();
bool dsidHasSys(TString dsid);
void fitRatioHists(std::map<TString, TH1*> rathistmap, std::map<TString, TH1*> histmap);
void plotCorrsAndSysts(TH1* hist, TLegend* leg,  std::vector<TH1*> &corrsyshistvec);




//
int plotset=-1; // this determines the set of plots to draw and steers the input files
std::map<int, TString> plotsetmap; // sets the pdf name for each plotset
std::map<int, StrV> plotsetdsidmap; // sets the pdf name for each plotset
std::map<int, StrV> plotsetdistmap; // sets the distributions for each plotset
std::map<int, StrV> plotsetbinmap; // sets the bins for each plotset
std::map<int, StrV> plotsetsysmap; // sets the systematic weights


// Input/output directories
TString indir="";
TString plotdir="";


// store in a map the pointers to the file objects
std::map<Str,TFile*> files;
// and map for directory other than indir
std::map<TString,TString> dirMap;
//  map dsid-filepath
std::map<TString,TString> filePathMap;
// map of dsid to sample description
std::map<TString, TString> dsidMap;
// map of dsid hist colours and line styles
std::map<TString,Color_t> colMap; // kRed,kBlue,kMagenta,kCyan
std::map<TString,unsigned int> lineMap; // kSolid = 1, kDashed= 2, kDotted= 3, kDashDotted = 4
void fillStyleMaps(TString dsid, TString title, unsigned int histWidth, Color_t histCol);
void setSysHistStyles(TH1* hist);

// canvas and pad settings
TCanvas* gcanv;
// histogram settings
float hist_LabelSizeY=0.05;
float hist_TitleSizeY=0.06;
float hist_TitleOffsetY=0.7;
// Two pads for ratio plots
TPad *P_1;
TPad *P_2;
// ratio plot settings (make any changes in plotset) 
// maximum range
float y_rat_max=1.49; // 1.99 
float y_rat_min=0.51;

// offsets/sizes of ratio histogram
float hist_rat_TitleSizeX=0.13;
float hist_rat_TitleOffsetX=0.95;
float hist_rat_LabelSizeX=0.1;

float hist_rat_TitleSizeY=0.09;
float hist_rat_TitleOffsetY=0.40; //0.77;
float hist_rat_LabelSizeY=0.08;

// and pads for ratio plot p1/p2 top/bottom
float p1_top_margin=0.08;
float p_left_margin=0.1; // left margin same for both pads

// some plotting defaults - can be overwritten below
bool doPlots=true;
bool doCutFlowPlots=false;
bool plotrat=true; // add ratio plot in bottom pad
// do log Y plot in addition
bool doLogY=false;
bool normToOne=false; // otherwise normalised plots are to the first histogram

// histogram x ranges
float mLL_min=81.0;
float mLL_max=100.99;
float mQQ_min=70.0;
float mQQ_max=110.0;
float mBB_min=60.0;
float mBB_max=150.0;
  
bool doXSecPlot=false; // xsec normalised
bool doShapePlot=true; // shape normalised
bool doCorrsAndSysts=false; // plot corrs and systs errors on top


void plotTruth3_Vjets() {

  // sets plotset to choose inputs/settings
  setInputOutput();
  setCanvasAndPads();
  if (doCorrsAndSysts)
    setCorrsAndSysts(); 

  // make standard plots 
  makePlots();

}

void setInputOutput() {

  TString dirstem="/afs/cern.ch/user/t/thompson/work/public/Hbb/TruthStudies_2021/run/";

 
  plotset=1; // ZZllbb
    
  if (plotset==1) {
    //
    plotsetmap[1]="sherpa_HF";
    //
    TString jobset="20210622_ZZllbb";

    filePathMap["950143"]=dirstem+jobset+"/"+"hist-mc15_13TeV.950143.Sh_2210_ZZ_llbb_EnhMaxPTV_valid.deriv.DAOD_TRUTH1.e8253_p4527.root";
    filePathMap["345044"]=dirstem+jobset+"/"+"hist-mc15_13TeV.345044.Sherpa_221_NNPDF30NNLO_ZbbZll.deriv.DAOD_TRUTH1.e6470_e5984_p4527.root";

    filePathMap["950143bfrw"]=dirstem+jobset+"/Sh2210_Brw/"+"hist-mc15_13TeV.950143.Sh_2210_ZZ_llbb_EnhMaxPTV_valid.deriv.DAOD_TRUTH1.e8253_p4527.root";
    //filePathMap["950143bfragrw"]=dirstem+jobset+"/Sh2210_BFragRw/"+"hist-mc15_13TeV.950143.Sh_2210_ZZ_llbb_EnhMaxPTV_valid.deriv.DAOD_TRUTH1.e8253_p4527.root";
    filePathMap["950143bfragrw"]=dirstem+jobset+"/Sh2210_BFRw_BFragRw/"+"hist-mc15_13TeV.950143.Sh_2210_ZZ_llbb_EnhMaxPTV_valid.deriv.DAOD_TRUTH1.e8253_p4527.root";

    filePathMap["361610"]=dirstem+jobset+"/"+"hist-mc15_13TeV.361610.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZqqll_mqq20mll20.deriv.DAOD_TRUTH1.e4711_p4527.root";

    // original order to get 950143->345044 bf and ptfrac reweights
    //plotsetdsidmap[1]=StrV({"950143","345044"});    
    //next order to get 950143bfrw->345044 ptfrac reweight
    //plotsetdsidmap[1]=StrV({"950143bfrw","345044","950143"});    
    // order for plotting
    plotsetdsidmap[1]=StrV({"345044","950143","950143bfrw","361610"});    

    //plotsetdsidmap[1]=StrV({"950143","345044","950143bfragrw"});    
    // plotsetdsidmap[1]=StrV({"950143","345044","361610"});    

    //
    plotsetdistmap[1]=StrV({"mbb","mbc","mbl","mcc","mcl","mll","mQQbb","mQQbc","mQQbl","mQQcc","mQQcl","mQQll"});

    // on the file
    //plotsetbinmap[1]=StrV({"2tag2jet_75_150ptv","2tag2jet_150ptv","2tag3jet_75_150ptv","2tag3jet_150ptv"});
    // do pTV merging
    plotsetbinmap[1]=StrV({"2tag2jet_75ptv"}); //pTV merged

    dsidMap["950143"]="Sh 2.2.10 Zllbb"; 
    lineMap["950143"]=2; 
    colMap["950143"]=kRed; 

    dsidMap["950143bfrw"]="Sh 2.2.10 Zllbb (BF rw)"; 
    lineMap["950143bfrw"]=4; 
    colMap["950143bfrw"]=kBlue; 

    dsidMap["950143bfragrw"]="Sh 2.2.10 Zllbb (B frag. rw)"; 
    lineMap["950143bfragrw"]=2; 
    colMap["950143bfragrw"]=kMagenta; 

    dsidMap["950147"]="Sh 2.2.10 llqq"; 
    lineMap["950147"]=1; 
    colMap["950147"]=kRed; 

    dsidMap["950144"]="Sh 2.2.10 Zllbb"; 
    lineMap["950144"]=1; 
    colMap["950144"]=kRed; 

    dsidMap["345044"]="Sh 2.2.1 ZllZbb"; 
    lineMap["345044"]=1; 
    colMap["345044"]=kBlack; 

    dsidMap["363356"]="Sherpa 2.2.1 ZllZqq";
    lineMap["363356"]=2; 
    colMap["363356"]=kBlack;

    dsidMap["361610"]="Powheg";
    lineMap["361610"]=3; 
    colMap["361610"]=kMagenta;

    normToOne=true;
    doShapePlot=true;

  } // end of plotsets


  // plotting directory for individual plots
  plotdir="plots_diboson"; // individual plots (to be done)

}

void setSysHistStyles(TH1* hist) {
  //
  // Set histogram styles for systematic variations
  //
  TString name=hist->GetName();
  bool meonly = (name.Contains("ME_ONLY")) ? true : false;
  if (meonly) {
    hist->SetLineColor(kBlue);
  } else {
    hist->SetLineColor(kRed);
  }
  if(name.Contains("MUR0.5_MUF0.5")) {
    hist->SetLineStyle(2);
  } else if(name.Contains("MUR0.5_MUF1")) {
    hist->SetLineStyle(3);
  } else if(name.Contains("MUR1_MUF0.5")) {
    hist->SetLineStyle(4);
  } else if(name.Contains("MUR1_MUF1")) {
    hist->SetLineStyle(1); // not usually used
  } else if(name.Contains("MUR1_MUF2")) {
    hist->SetLineStyle(2);
  } else if(name.Contains("MUR2_MUF1")) {
    hist->SetLineStyle(3);
  } else if(name.Contains("MUR2_MUF2")) {
    hist->SetLineStyle(4);
  }
}

void fillStyleMaps(TString dsid, TString title, unsigned int histWidth, Color_t histCol) {
  //
  dsidMap[dsid]=title;  
  lineMap[dsid]=histWidth; 
  colMap[dsid]=histCol; 
}

void setCanvasAndPads() {

  gcanv=new TCanvas("","",900,700);
  float ratfrac =0.35; // fraction of canvas devoted to ratio plot
  P_1 = new TPad("","", 0, ratfrac, 1.0, 1.0);
  P_2 = new TPad("","", 0, 0.0, 1.0, ratfrac);
  gStyle->SetOptStat(0);
  gcanv->Draw();
  P_2->Draw(); // bottom
  P_1->Draw(); // top
  P_2->SetLeftMargin(p_left_margin);
  P_2->SetBottomMargin(ratfrac);
  P_2->SetTopMargin(0.0);
  P_2->SetRightMargin(0.04);
  P_1->SetLeftMargin(p_left_margin);
  P_1->SetTopMargin(p1_top_margin);
  P_1->SetBottomMargin(0.0);
  P_1->SetRightMargin(0.04);
  
}

void setupRatioHist(TH1* hist, TString ytitle) {

  hist->SetLineWidth(3);
  hist->SetTitle("");
  hist->SetMaximum(y_rat_max);
  hist->SetMinimum(y_rat_min);
  hist->GetXaxis()->SetTitleSize(hist_rat_TitleSizeX);
  hist->GetXaxis()->SetTitleOffset(hist_rat_TitleOffsetX);
  hist->GetXaxis()->SetLabelSize(hist_rat_LabelSizeX);
  hist->GetYaxis()->SetTitleSize(hist_rat_TitleSizeY);
  hist->GetYaxis()->SetTitleOffset(hist_rat_TitleOffsetY);
  hist->GetYaxis()->SetLabelSize(hist_rat_LabelSizeY);
  hist->GetYaxis()->SetTitle(ytitle);
}

void setTitleRangeRebin(TH1* hist, TString ytitle) {


  TString hname=hist->GetName();

  // rebin
  unsigned int rebin= 
    hname.Contains("mbb") ?  2 : 
    hname.Contains("_mQQ") ?  1:    //    5 : 
    hname.Contains("_mLL") ? 1  :
    hname.Contains("_dRBB") ? 2 :
    1;

  hist->Rebin(rebin);

  // Ranges and titles
  if( hname.Contains("_mLLBB")) {
    //hist->SetAxisRange(0,1000,"X");
    hist->SetAxisRange(100,600,"X");
    hist->SetTitle(";m_{LLBB} [GeV] ;"+ytitle);
  }  else if( hname.Contains("_mLL")) {
    hist->SetAxisRange(mLL_min,mLL_max,"X");
    hist->SetTitle(";m_{LL} [GeV] ;"+ytitle);
  }else if(hname.Contains("_mBB")) {
    hist->SetAxisRange(mBB_min, mBB_max,"X");
    hist->SetTitle(";m_{BB}  [GeV] ;"+ytitle);
  }else if(hname.Contains("mbb")) {
    hist->SetAxisRange(mBB_min, mBB_max,"X");
    hist->SetTitle(";m_{BB}  [GeV] ;"+ytitle);
  }else if(hname.Contains("_mbc")) {
    hist->SetAxisRange(mBB_min, mBB_max,"X");
    hist->SetTitle(";m_{BC}  [GeV] ;"+ytitle);
  }else if(hname.Contains("_mbl")) {
    hist->SetAxisRange(mBB_min, mBB_max,"X");
    hist->SetTitle(";m_{BL}  [GeV] ;"+ytitle);
  }else if(hname.Contains("_mcc")) {
    hist->SetAxisRange(mBB_min, mBB_max,"X");
    hist->SetTitle(";m_{CC}  [GeV] ;"+ytitle);
  }else if(hname.Contains("_mcl")) {
    hist->SetAxisRange(mBB_min, mBB_max,"X");
    hist->SetTitle(";m_{CL}  [GeV] ;"+ytitle);
  }else if(hname.Contains("_mll")) {
    hist->SetAxisRange(mBB_min, mBB_max,"X");
    hist->SetTitle(";m_{LL}  [GeV] ;"+ytitle);
  }else if(hname.Contains("_mQQ")) {

    hist->SetAxisRange(mQQ_min,mQQ_max,"X");
    if(hname.Contains("_mQQll")) {
      hist->SetTitle(";m_{ll}  [GeV] ;"+ytitle);
    } else if(hname.Contains("_mQQcc")) {
      hist->SetTitle(";m_{cc}  [GeV] ;"+ytitle);
    } else if(hname.Contains("_mQQcl")) {
      hist->SetTitle(";m_{cl}  [GeV] ;"+ytitle);
    } else if(hname.Contains("_mQQbb")) {
      hist->SetTitle(";m_{bb}  [GeV] ;"+ytitle);
    } else if(hname.Contains("_mQQbc")) {
      hist->SetTitle(";m_{bc}  [GeV] ;"+ytitle);
    } else if(hname.Contains("_mQQbl")) {
      hist->SetTitle(";m_{bl}  [GeV] ;"+ytitle);
    } else {
      hist->SetTitle(";m_{QQ}  [GeV] ;"+ytitle);
    }
  }else if(hname.Contains("_pTV")) {
    hist->SetAxisRange(0,300,"X");
    hist->SetTitle(";p_{T}^{V}  [GeV] ;"+ytitle);
  }else if(hname.Contains("_pTB1")) {
    hist->SetAxisRange(20,300,"X");
    hist->SetTitle(";p_{T}^{B1}  [GeV] ;"+ytitle);
  }else if(hname.Contains("_dRBB")) {
    hist->SetAxisRange(0.5,3.1,"X");
    hist->SetTitle(";#Delta(R)^{BB} ;"+ytitle);
  } else {
    hist->GetYaxis()->SetTitle(ytitle);
  }


}



void makePlots() {


  // datasets to plot
  StrV dsids(plotsetdsidmap[plotset]); 
  
  // fill the map of pointers to the file objects
  for (auto dsid:dsids) { 
    
    // get the dsid of the file
    TString dsid_use=dsid(0,6);
    cout << "dsid:" << dsid << ", dsid of file:" << dsid_use << ":" << endl;
    
    // Add the file to the map
    TString filein=filePathMap[dsid];
    files[dsid]=openFile(filein);
    cout << " dsid " << dsid << " file:" << filein << " pointer " << files[dsid] << endl;
    files[dsid]->ls();
  }
  
    
  // Plots  
  // set .pdf file
  Str pdf("VHbbTruth_VV_"+plotsetmap[plotset]+".pdf");
  TCanvas *can = gcanv;//new TCanvas();
  can->Print(pdf+"[");
  //TPad* canpad = can->Pad();
  
  bool indplots=false;  // individual ps files as well as pdf (to be completed)

  // Set up histograms to plot starting with channel/process
  TString stem("hist_2Lep_ZZ");
  
  // analysis bins
  StrV bins(plotsetbinmap[plotset]);
  // regions
  StrV regions({"SR"});

  // distributions
  StrV distribs(plotsetdistmap[plotset]);
  TString syst("Nominal");

  // merge histograms e.g. across pTV bins
  // mergeHistograms(dsids, stem, bins, regions, distribs, syst);

  TLegend* legRight = new TLegend(0.65, 0.39, 0.95, 0.7);
  legRight -> SetFillStyle(0);
  legRight -> SetBorderSize(0);
  legRight -> SetTextSize(0.04);

  TLegend* legLeft = new TLegend(0.15, 0.39, 0.45, 0.7);
  legLeft -> SetFillStyle(0);
  legLeft -> SetBorderSize(0);
  legLeft -> SetTextSize(0.04);

  StrV histnames({"bPtFrac","bDecays","mbb"});

  if (doPlots) {
    
    //for (auto var:distribs) {
    //for (auto bin:bins) {
    //	for (auto region:regions) {
    for (auto histname : histnames) {

      TLegend* leg = legRight;
      if (histname.Contains("bPtFrac")) {
	leg=legLeft;
      }

      // 
      P_1->SetLogy(0); P_2->SetLogy(0); gPad-> SetLogy(0);
      // cross section normalised
      if (doXSecPlot) {
	//plotHistograms(dsids, stem, bin, region, var, syst, leg, plotrat, false);
	leg->Draw();
	can->Print(pdf);
	//if (indplots)
	//  can->Print(plotdir+var+bin+region+".eps");
	if (doLogY) {	      
	  P_1->SetLogy(1); gPad->SetLogy(1);
	  can->Print(pdf);
	  //if (indplots)
	  //  can->Print(plotdir+var+bin+region+"Log.eps");
	}
      }
      // shape normalised
      if (doShapePlot) {
	P_1->SetLogy(0); P_2->SetLogy(0); gPad-> SetLogy(0);	    
	//plotHistograms(dsids, stem, bin, region, var, syst, leg, plotrat, true);
	//void plotHistograms(TString histname, StrV dsids, TLegend* leg, TString legTitle, bool plotrat, 
	//	    bool doShape, TH1* recHist=0);
	TString legTitle(histname);
	cout << "hhhhhhh" << histname << endl;
	plotHistograms(histname, dsids, leg, legTitle, plotrat, true); 
	leg->Draw();
	can->Print(pdf);
	    //if (indplots)
	    //  can->Print(plotdir+var+bin+region+"Norm.eps");
	if (doLogY) {	      
	  P_1->SetLogy(1); gPad->SetLogy(1);
	  can->Print(pdf);
	      //if (indplots)
	      //  can->Print(plotdir+var+bin+region+"Log.eps");
	}
      } // shape plot
      //   } //regions
      //  } // bins
      if (doCorrsAndSysts)
	leg->Clear();
      // } // distributions    
    } // histnames
  } // do plots
  can->Print(pdf+"]");
  printf("\nProduced %s\n\n",pdf.Data());

}


void plotHistograms(StrV dsids, TString stem, TString bin, TString region, TString var, TString syst, 
		    TLegend* leg, bool plotrat, bool doShape) {

  TString histname = stem + "_" + bin + "_" + region + "_" + var + "_" + syst;
  TString legTitle(bin);
  cout << "plotHistograms A " << histname << endl;


  plotHistograms(histname, dsids, leg, legTitle, plotrat, doShape); 
  
}

void plotHistograms(TString histname, StrV dsids, TLegend* leg, TString legTitle, bool plotrat, bool doShape) {

  //
  bool first=true;
  bool firstRat=true;
  TH1F* histNorm=0;
  float norm=1.0;
  float ymax=-999;

  bool drawErrors=true;
  bool drawSysErrors=false; // errors on the systematics

  // see if there are any systematics to plot
  unsigned int nsys = (plotsetsysmap.count(plotset)) ? plotsetsysmap[plotset].size() : 0;

  // first loop to fill vector of hists to plot
  // Also set normalisations and get y maximum 
  // map of dsid to histogram
  map<TString, TH1*> histmap;
  map<TString, TH1*> rathistmap;
  map<TString, std::vector<TH1*>> syshistvecmap;

  cout << "plotHistograms B " << histname << endl;
  for (auto dsid:dsids) {	
    TFile *f=files[dsid];
    
    // loop - always nominal first, any systematics after
    unsigned int nloops=1+nsys;
    vector<TH1*> syshistsvec; //vector of systematic hists
    // first loop isys=0 is nominal
    for (int isys(0); isys<nloops ; ++isys) {
      
      TString histnameuse(histname);
      if (dsidHasSys(dsid) && isys>0) {
	TString sysname=plotsetsysmap[plotset].at(isys-1);
	//cout << "sysname " << sysname << endl;
	histnameuse = histnameuse.ReplaceAll("Nominal",sysname);
	histnameuse = histnameuse.Prepend("WeightVariations/");
	//cout << "histnameuse " << histnameuse << endl;
      }
      auto h=getHist(f,histnameuse);
      cout << " found " << h << " " << histnameuse << "" << endl;
      // put unavoidable histogram fixes here
      // end of fixes

      if (first) {
	first=false;
	norm = (normToOne) ? 1.0 : h->Integral();
	// extra
	/*
	TAxis *axis = h->GetXaxis();
	int bmin = axis->FindBin(60.0); 
	int bmax = axis->FindBin(155.0); 
	norm = h->Integral(bmin,bmax);
	*/
	//cout << dsid << " flav " << histnameuse << "  " << norm << endl;
      }

      // shape normalised (norm to first dsid or 1.0)
      if (doShape) {
	float sumw=h->Integral();
	// extra
	//TAxis *axis = h->GetXaxis();
	//int bmin = axis->FindBin(60.0); 
	//int bmax = axis->FindBin(155.0); 
	//sumw = h->Integral(bmin,bmax);
	sumw = h->Integral();
	cout << dsid << " flav " << histnameuse << "  " << sumw << endl;
	float scalefac=norm/sumw;
	cout << " shape norm " << scalefac << " " << norm << " " << sumw << endl; 
	h->Scale(scalefac);
      }
      TString ytitle = (doShape) ? "Events (Shape norm.)" : "Events (Xsec norm.)";
      setTitleRangeRebin(h, ytitle); // modify title, x-bins and rebinning

      // store maximum y-range
      float h_ymax = h->GetMaximum();
      if (dsid=="345044"&&histname.Contains("weightNomWide")) {
	h_ymax=-10;
	h->SetMinimum(-10.0);
      }
      if ( h_ymax > ymax)
	ymax = h_ymax;
      
      if (isys==0) 
	histmap[dsid]=h;
      else {
	syshistsvec.push_back(h);
      } 
    } //nominal + systematic loop      
    syshistvecmap[dsid]=syshistsvec;
  } // end of dsid loop


  // plotting loop over dsid-hist map
  first=true;
  std::vector<TH1*> corrsyshistvec;
  for (auto dsid:dsids) {	
    auto h=histmap[dsid];

    TString drawOpt = "hist same";

    if (first) {
      drawOpt = "hist";
      // first=false; // set below
      h->GetYaxis()->SetLabelSize(hist_LabelSizeY);
      h->GetYaxis()->SetTitleSize(hist_TitleSizeY);
      h->GetYaxis()->SetTitleOffset(hist_TitleOffsetY);
      h->SetMaximum(1.1*ymax);
    }
    cout << dsid << " " << histname << " " << first << " " << drawOpt << " " << h->GetEntries() << " " << h->GetSumOfWeights() << endl;//" max "

    // ratio plot, go to the top pad first
    if (plotrat) {
      P_1->cd();
    }
    
    drawHist(h,drawOpt, colMap[dsid], lineMap[dsid]);
    if (drawErrors) 
      drawHist(h,"same e", colMap[dsid], lineMap[dsid]);

    // make the bin the legend title and add "Norm" if shape normalised
    TString legtitle=legTitle;
    if (doShape)
      legtitle+=" (Norm)";
    if (first)
      leg->SetHeader(legtitle);
    // only add legend entry if not done before
    addLegendEntry(leg, h, dsidMap[dsid]); // see histutils.h
    

    // plot Corrs And Systs systematics for first histogram
    if (doCorrsAndSysts && first)
      plotCorrsAndSysts(h,leg,corrsyshistvec);
    
    // plot systematics from Weight Variations
    if (dsidHasSys(dsid) && nsys>0) {
      vector<TH1*> syshistsvec=syshistvecmap[dsid];
      int isys(0);
      for (auto hsys: syshistsvec) {
	//cout << " hsys " << hsys->GetName() << endl;
	hsys->SetLineColor(kRed);
	setSysHistStyles(hsys);
	hsys->Draw("same hist");
	if (drawSysErrors)
	  hsys->Draw("same e");
	//cout << "isys " << isys << " plotset " << plotset << " " << plotsetsysmap[plotset].size() <<  endl;
	TString sysname=plotsetsysmap[plotset].at(isys);
	//cout << "sysname(systematic legend " << sysname << endl;
	addLegendEntry(leg, hsys, sysname); 
	isys++;
      }
    }


    if (first) {
      first=false;
    }

    // test Peterson fragmentation fit
    //    if (h && TString(h->GetName())=="bPtFrac") {

    // }
    // ratio plot in bottom panel
    if (plotrat) {
      if (firstRat) {
	// normalise to first histogram in the array
	histNorm=(TH1F*)h->Clone();
	//firstRat=false; // set below
      }
      P_2->cd();
      TH1F* histR=(TH1F*)h->Clone();
      histR->Divide(histNorm);
      setupRatioHist(histR);     
      histR->Draw(drawOpt);
      rathistmap[dsid]=histR;
      if (drawErrors) 
	histR->Draw("same e");
      
      // draw corrs and systs in ratio
      //cout << "corrsyshistvec.size() " << corrsyshistvec.size() << endl;
      if (corrsyshistvec.size()>0) {
	for (auto hsys: corrsyshistvec) {
	  TH1F* histR=(TH1F*)hsys->Clone();
	  histR->Divide(histNorm);
	  setupRatioHist(histR);     
	  histR->Draw("same hist");
	}
      }


      // draw systematics in ratio
      if (nsys>0) {
	vector<TH1*> syshistsvec=syshistvecmap[dsid];
	for (auto hsys: syshistsvec) {
	  TH1F* histR=(TH1F*)hsys->Clone();
	  histR->Divide(histNorm);
	  setupRatioHist(histR);     
	  histR->Draw("same hist");
	  if (drawSysErrors) 
	    histR->Draw("same e1");
	}
      }

      if (firstRat) 
	firstRat=false;

      // 
    } // end of ratio plot    

  } //dsid plotting loop    
  // for the legend want to return in P_1
  if (plotrat) {
    fitRatioHists(rathistmap, histmap);
    P_1->cd();
  }

}

bool dsidHasSys(TString dsid) {
  // See if sample has the systematic histograms
  // only Sherpa validation samples have systematics
  bool hasSys=dsid.Contains("9501"); 
  return hasSys;
}

void plotCorrsAndSysts(TH1* hist, TLegend* leg,  std::vector<TH1*> &corrsyshistvec) {
  //
  // use hist to make systematic weight histograms and add them to corrsyshistvec
  if (!hist) return;
  TString histname=hist->GetName();
  cout << " " << hist->GetName() << endl;
  bool is2jet = histname.Contains("2tag2jet_");

  TH1* hsysdo=(TH1*)hist->Clone();
  TH1* hsysup=(TH1*)hist->Clone();
  
  corrsyshistvec.clear();
  if (histname.Contains("SR_mBB")) {
    std::shared_ptr<TF1>  mbbmesys = (is2jet) ? m_f_SysVVMbbME_ZZllqq_2j : m_f_SysVVMbbME_ZZllqq_3j;
    getCorrsAndSystsHists(hsysup, hsysdo, mbbmesys, 1000); // see CorrsAndSysts.h
    //cout << "hist " << hist << " " << hist->Integral() << endl;
    //cout << "hsysup " << hsysup << " " << hsysup->Integral() << endl;
    //cout << "hsysdo " << hsysdo << " " << hsysdo->Integral() << endl;
    drawHist(hsysup,"same hist", kMagenta, 3);
    drawHist(hsysdo,"same hist", kMagenta, 4);
    addLegendEntry(leg, hsysup, "SysVVMBBMEUp"); // see histutil.h
    addLegendEntry(leg, hsysdo, "SysVVMBBMEDo"); // see histutil.h
    corrsyshistvec.push_back(hsysup);
    corrsyshistvec.push_back(hsysdo);
  }
  if (histname.Contains("SR_pTV")) {
    std::shared_ptr<TF1>  ptvmesys = (is2jet) ? m_f_SysVVPTVME_ZZllqq_2j : m_f_SysVVPTVME_ZZllqq_3j;
    getCorrsAndSystsHists(hsysup, hsysdo, ptvmesys, 1000); // see CorrsAndSysts.h
    drawHist(hsysup,"same hist", kMagenta, 3);
    drawHist(hsysdo,"same hist", kMagenta, 4);
    addLegendEntry(leg, hsysup, "SysVVPTVMEUp"); // see histutil.h
    addLegendEntry(leg, hsysdo, "SysVVPTVMEDo"); // see histutil.h
    corrsyshistvec.push_back(hsysup);
    corrsyshistvec.push_back(hsysdo);
    //
    hsysdo=(TH1*)hist->Clone();
    hsysup=(TH1*)hist->Clone();
    std::shared_ptr<TF1>  ptvpsuesys = (is2jet) ? m_f_SysVVPTVPSUE_ZZllqq_2j : m_f_SysVVPTVPSUE_ZZllqq_3j;
    getCorrsAndSystsHists(hsysup, hsysdo, ptvpsuesys, 1000); // see CorrsAndSysts.h
    drawHist(hsysup,"same hist", kMagenta, 2);
    drawHist(hsysdo,"same hist", kMagenta, 5);
    addLegendEntry(leg, hsysup, "SysVVPTVPSUEUp"); // see histutil.h
    addLegendEntry(leg, hsysdo, "SysVVPTVPSUEDo"); // see histutil.h
    corrsyshistvec.push_back(hsysup);
    corrsyshistvec.push_back(hsysdo);

  }

}

void fitRatioHists(std::map<TString, TH1*> rathistmap, std::map<TString, TH1*> histmap) {
  //
  // Fit ratio histograms - and maybe standard histograms
  //
  // Sometimes easier to make pol function by hand
  TF1 *pol2_h1 = new TF1("pol2_hist1","[0]+[1]*x+[2]*x*x",-3,3);
  TF1 *pol2_h2 = new TF1("pol2_hist2","[0]+[1]*x+[2]*x*x",-3,3);

  if (plotset==1) {

    //
    TH1* rathist1= (rathistmap["345044"]) ? (TH1*)rathistmap["345044"]->Clone() : 0;
    TH1* rathist2= (rathistmap["950143"]) ? (TH1*)rathistmap["950143"]->Clone() : 0;

    TH1* hist1= (histmap["345044"]) ? (TH1*)histmap["345044"]->Clone() : 0;
    TH1* hist2= (histmap["950143"]) ? (TH1*)histmap["950143"]->Clone() : 0;

    // get a histogram of fragmentation ratio
    if (rathist1 && TString(rathist1->GetName())=="bPtFrac") {

      //rathist1->Fit("chebyshev4","","same");
      for (int i(1); i<=rathist1->GetNbinsX() ; i++) {
	float frac=rathist1->GetBinContent(i);
	//cout << rathist1->GetBinCenter(i) << " " << frac << endl;
      }
      TFile* file=TFile::Open("ratiohists.root","RECREATE");
      file->cd();
      rathist1->Write();
      file->ls();
      file->Close();

    }
    // some failed attempts at fitting Peterson fragmentation
    if (hist1 && TString(hist1->GetName())=="bPtFracccc") {

      TF1 *func = new TF1("fit",fitPeteF,0,1.2,2);
      // set the parameters to the mean and RMS of the histogram
      func->SetParameters(100,0.006);

      // give the parameters meaningful names
      func->SetParNames ("Constant","epsilon");

      // call TH1::Fit with the name of the TF1 object
      hist1->Fit("fit","R","same");
      TF1* func1= new TF1("fit1",fitPeteF,0,1.2,2);
      func1->SetParameters(func->GetParameters());

      hist2->Fit("fit","R","same");
      TF1* func2= new TF1("fit2",fitPeteF,0,1.2,2);
      func2->SetParameters(func->GetParameters());

      cout << "func1 pars:" << func1->GetParameter(0) << " " << func1->GetParameter(1) << endl;;
      cout << "func2 pars:" << func2->GetParameter(0) << " " << func2->GetParameter(1) << endl;;

      TF1* funcRat= new TF1("fit2",fitPeteFRatio,0,1.2,4);
      funcRat->SetParameter(0,func1->GetParameter(0));
      funcRat->SetParameter(1,func1->GetParameter(1));
      funcRat->SetParameter(2,func2->GetParameter(0));
      funcRat->SetParameter(3,func2->GetParameter(1));

      funcRat->Draw("same");

    }
    if (rathist1 && TString(rathist1->GetName())=="bDecays") {
      cout << "hadron   Sherpa 2.1.1   Sherpa 2.2.10" << endl;
      for (int i(0); i<=rathist1->GetNbinsX() ; i++) {
	float frac=rathist1->GetBinContent(i);
	float frac2=(rathist2) ? rathist2->GetBinContent(i) : 0;
	cout << i << " " << frac << " " << frac2 << endl;
      
      }
    }
    /*
    pol2_h1->SetRange(81,101);
    pol2_h1->SetLineColor(kMagenta);

    pol2_h2->SetRange(81,101);
    pol2_h2->SetLineColor(kYellow);

    hist1->Divide(hist);
    hist2->Divide(hist);
    P_2->cd();
    //cout << "fit 1" << endl;
    hist1->Fit("pol2_hist1","R","same");
    //cout << "fit 2" << endl;
    hist2->Fit("pol2_hist2","R","same");
    */

  }

}

void  mergeHistograms(StrV dsids, TString stem, StrV bins, StrV regions, StrV distribs, TString syst) {

  bool merge_2tag2jet_75ptv=false;
  bool merge_2tag3jet_75ptv=false;
  for (auto bin: bins) 
    if (bin.Contains("2tag2jet_75ptv")) 
      merge_2tag2jet_75ptv=true;
  for (auto bin: bins) 
    if (bin.Contains("2tag3jet_75ptv")) 
      merge_2tag3jet_75ptv=true;

  bool merge_2tag2jet_0ptv=false;
  bool merge_2tag3jet_0ptv=false;
  for (auto bin: bins) 
    if (bin.Contains("2tag2jet_0ptv")) 
      merge_2tag2jet_0ptv=true;
  for (auto bin: bins) 
    if (bin.Contains("2tag3jet_0ptv")) 
      merge_2tag3jet_0ptv=true;

  if (!merge_2tag2jet_0ptv && !merge_2tag3jet_0ptv && !merge_2tag2jet_75ptv && !merge_2tag3jet_75ptv) return;


  StrV ptv0bins2Jet({"2tag2jet_0_75ptv","2tag2jet_75_150ptv","2tag2jet_150ptv"});
  StrV ptv0bins3Jet({"2tag2jet_0_75ptv","2tag3jet_75_150ptv","2tag3jet_150ptv"});

  StrV ptv75bins2Jet({"2tag2jet_75_150ptv","2tag2jet_150ptv"});
  StrV ptv75bins3Jet({"2tag3jet_75_150ptv","2tag3jet_150ptv"});

  std::map <TString, StrV> binsToMerge;

  if (merge_2tag2jet_75ptv)
    binsToMerge["2tag2jet_75ptv"]=ptv75bins2Jet;
  if (merge_2tag3jet_75ptv)
    binsToMerge["2tag3jet_75ptv"]=ptv75bins3Jet;

  if (merge_2tag2jet_0ptv)
    binsToMerge["2tag2jet_0ptv"]=ptv0bins2Jet;
  if (merge_2tag3jet_0ptv)
    binsToMerge["2tag3jet_0ptv"]=ptv0bins3Jet;

  for ( const auto &mergeMap :  binsToMerge ) {
    std::cout << "merge histogram:" <<  mergeMap.first << " merges:" << endl; 
    for (auto &histToMerge :  mergeMap.second) {
      cout << histToMerge << endl;
    }
  }
  for (auto var:distribs) {
    for (auto region:regions) {
      for (auto dsid:dsids) {	
	TFile *f=files[dsid];
	f->cd(); // to save the merged historgram in the file memory
	for ( const auto &mergeMap :  binsToMerge ) {
	  TString histnameMerged = stem + "_" +  mergeMap.first + "_" + region + "_" + var + "_" + syst;
	  TH1* mergedHist=0;
	  for (auto &histToMerge :  mergeMap.second) {
	    TString histname = stem + "_" + histToMerge + "_" + region + "_" + var + "_" + syst;
	    //cout << " " << histnameMerged  << " " << histname << endl;
	    auto h=getHist(f,histname);
	    if(!mergedHist && h)
	      mergedHist=(TH1*)h->Clone(histnameMerged);
	    else if (mergedHist && h)
	      mergedHist->Add(h);
	  }
	  //cout << "merged hist dir " << mergedHist->GetDirectory()->GetName() << endl; 
	} // merged map
     } // dsids
    } // regions
  } // distributions

}
